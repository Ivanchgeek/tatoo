package structs

//DesktopText - huge text box's content
type DesktopText struct {
	Tag                string
	HugeDesktopText    string
	RegularDesktopText string
}

//MobileText - huge mobile box's content
type MobileText struct {
	Tag               string
	HugeMobileText    string
	RegularMobileText string
}

//Links - footer content
type Links struct {
	Tag         string
	Vk          string
	Inst        string
	EMail       string
	PhoneFace   string
	PhoneLink   string
	AddressFace string
	AddressLink string
}

//Images - images associations
type Images struct {
	Tag    string
	Images []string
}
