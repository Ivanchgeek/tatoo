package bot

import (
	"log"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"gitlab.com/Ivanchgeek/tatoo/backend/pkg/config"
)

var bot *tgbotapi.BotAPI

//Init - init bot
func Init() {
	botBuff, err := tgbotapi.NewBotAPI(config.Get().BotToken)
	if err != nil {
		log.Panic(err)
	}
	bot = botBuff
	bot.Debug = config.Get().BotDebug
	log.Printf("Authorized on account %s", bot.Self.UserName)
}

//SendOrder - send message to admins
func SendOrder(name string, phone string) {
	send("⭐⭐⭐*Новый заказ*⭐⭐⭐\n*Имя*: " + name + "\n*Телефон*:" + phone)
}

//SendError - send error message to admins
func SendError(message string) {
	send("❗❗❗*ERROR*❗❗❗\n" + message)
}

func send(message string) {
	for i := 0; i < len(config.Get().AdminsIds); i++ {
		msg := tgbotapi.NewMessage(config.Get().AdminsIds[i], message)
		msg.ParseMode = "markdown"
		bot.Send(msg)
	}
}
