package db

import (
	"log"

	"gitlab.com/Ivanchgeek/tatoo/backend/pkg/config"
	"gitlab.com/Ivanchgeek/tatoo/backend/pkg/structs"
	"gopkg.in/mgo.v2/bson"
)

//GetDesktopText - get desktop text from db
func GetDesktopText() (structs.DesktopText, error) {
	sess := session.Clone()
	defer sess.Close()
	var result structs.DesktopText
	err := sess.DB(config.Get().DBName).C(config.Get().TextCollection).Find(bson.M{"tag": config.Get().DesktopTextTag}).One(&result)
	if err != nil {
		return structs.DesktopText{}, err
	}
	return result, nil
}

//SetDesktopText - update desktop text
func SetDesktopText(HugeDesktopText string, RegularDesktopText string) error {
	sess := session.Clone()
	defer sess.Close()
	_, err := sess.DB(config.Get().DBName).C(config.Get().TextCollection).Upsert(bson.M{"tag": config.Get().DesktopTextTag}, structs.DesktopText{
		Tag:                config.Get().DesktopTextTag,
		HugeDesktopText:    HugeDesktopText,
		RegularDesktopText: RegularDesktopText,
	})
	if err == nil {
		log.Println("desktop text updated")
	}
	return err
}

//GetMobileText - get mobile text from db
func GetMobileText() (structs.MobileText, error) {
	sess := session.Clone()
	defer sess.Close()
	var result structs.MobileText
	err := sess.DB(config.Get().DBName).C(config.Get().TextCollection).Find(bson.M{"tag": config.Get().MobileTextTag}).One(&result)
	if err != nil {
		return structs.MobileText{}, err
	}
	return result, nil
}

//SetMobileText - update mobile text
func SetMobileText(HugeMobileText string, RegularMobileText string) error {
	sess := session.Clone()
	defer sess.Close()
	_, err := sess.DB(config.Get().DBName).C(config.Get().TextCollection).Upsert(bson.M{"tag": config.Get().MobileTextTag}, structs.MobileText{
		Tag:               config.Get().MobileTextTag,
		HugeMobileText:    HugeMobileText,
		RegularMobileText: RegularMobileText,
	})
	if err == nil {
		log.Println("mobile text updated")
	}
	return err
}

//GetLinks - get links for footer
func GetLinks() (structs.Links, error) {
	sess := session.Clone()
	defer sess.Close()
	var result structs.Links
	err := sess.DB(config.Get().DBName).C(config.Get().TextCollection).Find(bson.M{"tag": config.Get().LinksTag}).One(&result)
	if err != nil {
		return structs.Links{}, err
	}
	return result, nil
}

//SetLinks - update footer links
func SetLinks(Vk string, Inst string, EMail string, PhoneFace string, PhoneLink string, AddressFace string, AddressLink string) error {
	sess := session.Clone()
	defer sess.Close()
	_, err := sess.DB(config.Get().DBName).C(config.Get().TextCollection).Upsert(bson.M{"tag": config.Get().LinksTag}, structs.Links{
		Tag:         config.Get().LinksTag,
		Vk:          Vk,
		Inst:        Inst,
		EMail:       EMail,
		PhoneFace:   PhoneFace,
		PhoneLink:   PhoneLink,
		AddressFace: AddressFace,
		AddressLink: AddressLink,
	})
	if err == nil {
		log.Println("links updated")
	}
	return err
}
