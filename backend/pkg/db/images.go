package db

import (
	"errors"
	"fmt"
	"log"

	"gitlab.com/Ivanchgeek/tatoo/backend/pkg/structs"

	"gitlab.com/Ivanchgeek/tatoo/backend/pkg/config"
	"gopkg.in/mgo.v2/bson"
)

//AssociateNewImages - associate new images uids to db
func AssociateNewImages(images []string) error {
	sess := session.Clone()
	defer sess.Close()
	_, err := sess.DB(config.Get().DBName).C(config.Get().ImagesCollection).Upsert(bson.M{"tag": config.Get().ImagesTag},
		bson.M{"$push": bson.M{"images": bson.M{"$each": images, "$position": 0}}})
	if err == nil {
		fmt.Println("new images associated")
	}
	return err
}

//GetImagesList - get images associations from db
func GetImagesList() ([]string, error) {
	sess := session.Clone()
	defer sess.Close()
	var result structs.Images
	err := sess.DB(config.Get().DBName).C(config.Get().ImagesCollection).Find(bson.M{"tag": config.Get().ImagesTag}).One(&result)
	if err != nil {
		return []string{}, err
	}
	return result.Images, nil
}

//RemoveImage - remove image's db association
func RemoveImage(image string) error {
	sess := session.Clone()
	defer sess.Close()
	err := sess.DB(config.Get().DBName).C(config.Get().ImagesCollection).Update(bson.M{"tag": config.Get().ImagesTag}, bson.M{"$pull": bson.M{"images": image}})
	if err == nil {
		log.Println("image removed")
	}
	return err
}

//MoveImage - move image association in array (direction true => up; else down)
func MoveImage(image string, direction bool) error {
	sess := session.Clone()
	defer sess.Close()
	var result structs.Images
	if err := sess.DB(config.Get().DBName).C(config.Get().ImagesCollection).Find(bson.M{"tag": config.Get().ImagesTag}).One(&result); err != nil {
		return err
	}
	imagesArray := result.Images
	position := -1
	for index, img := range imagesArray {
		if img == image {
			position = index
			break
		}
	}
	if position == -1 {
		return errors.New("no such image")
	}
	if (position == len(imagesArray)-1 && !direction) || (position == 0 && direction) {
		return errors.New("no space")
	}
	value := imagesArray[position]
	imagesArray = append(imagesArray[:position], imagesArray[position+1:]...)
	if direction {
		imagesArray = insert(imagesArray, position-1, value)
	} else {
		imagesArray = insert(imagesArray, position+1, value)
	}
	err := sess.DB(config.Get().DBName).C(config.Get().ImagesCollection).Update(bson.M{"tag": config.Get().ImagesTag}, bson.M{"$set": bson.M{"images": imagesArray}})
	if err == nil {
		log.Println("image moved")
	}
	return err
}

func insert(a []string, index int, value string) []string {
	if len(a) == index {
		return append(a, value)
	}
	a = append(a[:index+1], a[index:]...)
	a[index] = value
	return a
}
