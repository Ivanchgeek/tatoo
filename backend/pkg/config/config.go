package config

import (
	"encoding/json"
	"log"
	"os"
)

//Configuration - to parse json config
type Configuration struct {
	Port             string
	AdminAccessKey   string
	BotToken         string
	BotDebug         bool
	AdminsIds        []int64
	DBName           string
	TextCollection   string
	DesktopTextTag   string
	MobileTextTag    string
	LinksTag         string
	ImagePath        string
	ImagesCollection string
	ImagesTag        string
}

var configuration *Configuration

//Init - initialize app configuration
func Init(name string) {
	file, err := os.Open(name)
	if err != nil {
		panic(err)
	}
	decoder := json.NewDecoder(file)
	err = decoder.Decode(&configuration)
	if err != nil {
		panic(err)
	}
	log.Println("configuration parsed")
}

//Get - return link to current configuration
func Get() *Configuration {
	return configuration
}
