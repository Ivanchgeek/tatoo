package handlers

import (
	"encoding/json"
	"net/http"

	"gitlab.com/Ivanchgeek/tatoo/backend/pkg/bot"
)

//SendOrder - send information about order to tg bot
func SendOrder(w http.ResponseWriter, r *http.Request) {
	var request struct {
		Name  string
		Phone string
	}
	err := json.NewDecoder(r.Body).Decode(&request)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	bot.SendOrder(request.Name, request.Phone)
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("ok"))
}
