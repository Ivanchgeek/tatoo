package handlers

import (
	"encoding/json"
	"io"
	"net/http"
	"os"
	"strconv"
	"strings"

	"gitlab.com/Ivanchgeek/tatoo/backend/pkg/db"

	"gitlab.com/Ivanchgeek/tatoo/backend/pkg/bot"

	"gitlab.com/Ivanchgeek/tatoo/backend/pkg/config"

	"github.com/google/uuid"
)

//LoadImages - post new galary images
func LoadImages(w http.ResponseWriter, r *http.Request) {
	if err := r.ParseMultipartForm(32 << 20); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	if r.FormValue("AccessKey") != config.Get().AdminAccessKey {
		http.Error(w, "permission denied", http.StatusMethodNotAllowed)
		return
	}
	length, _ := strconv.Atoi(r.FormValue("length"))
	var savedImages []string
	for i := 0; i < length; i++ {
		file, _, err := r.FormFile(strconv.FormatInt(int64(i), 10))
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		defer file.Close()
		uid := uuid.New().String()
		writer, err := os.OpenFile(config.Get().ImagePath+uid+".jpg", os.O_WRONLY|os.O_CREATE, 0666)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			bot.SendError("error on creating file " + uid + ".jpg\n" + err.Error())
			defer removeSavedImagesOnError(savedImages)
			return
		}
		defer writer.Close()
		_, err = io.Copy(writer, file)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			bot.SendError("error on saving image " + uid + ".jpg\n" + err.Error())
			defer removeSavedImagesOnError(savedImages)
			return
		}
		savedImages = append(savedImages, uid+".jpg")
	}
	if err := db.AssociateNewImages(savedImages); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		bot.SendError("error on associating images to db " + strings.Join(savedImages, ", ") + "\n" + err.Error())
		defer removeSavedImagesOnError(savedImages)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(""))
}

func removeSavedImagesOnError(remove []string) {
	for _, file := range remove {
		if err := os.Remove(config.Get().ImagePath + file); err != nil {
			bot.SendError("problems on removing file " + file + "\n" + err.Error())
		}

	}
}

//GetImagesList - get images assotiations
func GetImagesList(w http.ResponseWriter, r *http.Request) {
	result, err := db.GetImagesList()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	js, err1 := json.Marshal(result)
	if err1 != nil {
		http.Error(w, err1.Error(), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}

//RemoveImage - removes image from image path and db
func RemoveImage(w http.ResponseWriter, r *http.Request) {
	var request struct {
		AccessKey string
		Image     string
	}
	err := json.NewDecoder(r.Body).Decode(&request)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if request.AccessKey == config.Get().AdminAccessKey {
		err1 := db.RemoveImage(request.Image)
		if err1 != nil {
			http.Error(w, err1.Error(), http.StatusInternalServerError)
			return
		}
		if err := os.Remove(config.Get().ImagePath + request.Image); err != nil {
			bot.SendError("problems on removing image file " + request.Image + "\n" + err.Error())
		}
	} else {
		http.Error(w, "permission denied", http.StatusMethodNotAllowed)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("ok"))
}

//MoveImage - move image in galary
func MoveImage(w http.ResponseWriter, r *http.Request) {
	var request struct {
		AccessKey string
		Image     string
		Direction bool
	}
	err := json.NewDecoder(r.Body).Decode(&request)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if request.AccessKey == config.Get().AdminAccessKey {
		err1 := db.MoveImage(request.Image, request.Direction)
		if err1 != nil {
			if err1.Error() == "no space" {
				http.Error(w, err1.Error(), http.StatusBadRequest)
				return
			}
			http.Error(w, err1.Error(), http.StatusInternalServerError)
			return
		}
	} else {
		http.Error(w, "permission denied", http.StatusMethodNotAllowed)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("ok"))
}
