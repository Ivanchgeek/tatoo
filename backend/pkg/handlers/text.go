package handlers

import (
	"encoding/json"
	"net/http"

	"gitlab.com/Ivanchgeek/tatoo/backend/pkg/config"

	"gitlab.com/Ivanchgeek/tatoo/backend/pkg/db"
)

//GetDesktopText - return desktop huge text box's content
func GetDesktopText(w http.ResponseWriter, r *http.Request) {
	result, err := db.GetDesktopText()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	js, err1 := json.Marshal(result)
	if err1 != nil {
		http.Error(w, err1.Error(), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}

//SetDesktopText - set desktop huge text box's content
func SetDesktopText(w http.ResponseWriter, r *http.Request) {
	var request struct {
		AccessKey          string
		HugeDesktopText    string
		RegularDesktopText string
	}
	err := json.NewDecoder(r.Body).Decode(&request)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if request.AccessKey == config.Get().AdminAccessKey {
		err1 := db.SetDesktopText(request.HugeDesktopText, request.RegularDesktopText)
		if err1 != nil {
			http.Error(w, err1.Error(), http.StatusInternalServerError)
			return
		}
	} else {
		http.Error(w, "permission denied", http.StatusMethodNotAllowed)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("ok"))
}

//GetMobileText - return mobile huge text box's content
func GetMobileText(w http.ResponseWriter, r *http.Request) {
	result, err := db.GetMobileText()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	js, err1 := json.Marshal(result)
	if err1 != nil {
		http.Error(w, err1.Error(), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}

//SetMobileText - set mobile huge text box's content
func SetMobileText(w http.ResponseWriter, r *http.Request) {
	var request struct {
		AccessKey         string
		HugeMobileText    string
		RegularMobileText string
	}
	err := json.NewDecoder(r.Body).Decode(&request)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if request.AccessKey == config.Get().AdminAccessKey {
		err1 := db.SetMobileText(request.HugeMobileText, request.RegularMobileText)
		if err1 != nil {
			http.Error(w, err1.Error(), http.StatusInternalServerError)
			return
		}
	} else {
		http.Error(w, "permission denied", http.StatusMethodNotAllowed)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("ok"))
}

//GetLinks - return footer links
func GetLinks(w http.ResponseWriter, r *http.Request) {
	result, err := db.GetLinks()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	js, err1 := json.Marshal(result)
	if err1 != nil {
		http.Error(w, err1.Error(), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}

//SetLinks - set footer links
func SetLinks(w http.ResponseWriter, r *http.Request) {
	var request struct {
		AccessKey   string
		Vk          string
		Inst        string
		EMail       string
		PhoneFace   string
		PhoneLink   string
		AddressFace string
		AddressLink string
	}
	err := json.NewDecoder(r.Body).Decode(&request)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if request.AccessKey == config.Get().AdminAccessKey {
		err1 := db.SetLinks(request.Vk, request.Inst, request.EMail, request.PhoneFace, request.PhoneLink, request.AddressFace, request.AddressLink)
		if err1 != nil {
			http.Error(w, err1.Error(), http.StatusInternalServerError)
			return
		}
	} else {
		http.Error(w, "permission denied", http.StatusMethodNotAllowed)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("ok"))
}
