package main

import (
	"flag"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/rs/cors"
	"gitlab.com/Ivanchgeek/tatoo/backend/pkg/bot"
	"gitlab.com/Ivanchgeek/tatoo/backend/pkg/config"
	"gitlab.com/Ivanchgeek/tatoo/backend/pkg/db"
	"gitlab.com/Ivanchgeek/tatoo/backend/pkg/handlers"
)

var (
	configFile string
)

func init() {
	flag.StringVar(&configFile, "conf", "./src/gitlab.com/Ivanchgeek/tatoo/backend/dev.json", "config file")
	flag.Parse()
	log.Println("setting up with " + configFile)
	config.Init(configFile)
	db.Init()
	bot.Init()
}

func main() {
	r := mux.NewRouter().StrictSlash(true)

	r.HandleFunc("/get/desktopText", handlers.GetDesktopText)
	r.HandleFunc("/set/desktopText", handlers.SetDesktopText)
	r.HandleFunc("/get/mobileText", handlers.GetMobileText)
	r.HandleFunc("/set/mobileText", handlers.SetMobileText)
	r.HandleFunc("/get/links", handlers.GetLinks)
	r.HandleFunc("/set/links", handlers.SetLinks)
	r.HandleFunc("/sendOrder", handlers.SendOrder)
	r.HandleFunc("/loadImages", handlers.LoadImages)
	r.PathPrefix("/image/").Handler(http.StripPrefix("/image/", http.FileServer(http.Dir(config.Get().ImagePath))))
	r.HandleFunc("/get/images", handlers.GetImagesList)
	r.HandleFunc("/remove/image", handlers.RemoveImage)
	r.HandleFunc("/move/image", handlers.MoveImage)

	if err := http.ListenAndServe(":"+config.Get().Port, cors.Default().Handler(r)); err != nil {
		panic(err)
	}

}
