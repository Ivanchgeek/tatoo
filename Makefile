prod:
	sudo docker-compose -f docker-compose.prod.yml build
	sudo docker-compose -f docker-compose.prod.yml up -d

down:
	sudo docker-compose -f docker-compose.prod.yml down

dev:
	cd backend && sudo go mod vendor
	docker-compose -f docker-compose.dev.yml build
	sudo docker-compose -f docker-compose.dev.yml up

prepare:
	mkdir mongodb
	mkdir backend/images

mongo-cli:
	sudo docker exec -it mongodb mongo