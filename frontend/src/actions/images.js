import {fetched} from "../utl/fetched"

export const LoadImagesList = () => {
    return dispatch => {
        fetched("/get/images", {
            method: "get"
        }).then(resp => {
            if (!resp.ok) {
                throw resp.statusText
            }
            return resp.json()
        }).then(js => {
            dispatch({type: "SetImages", Images: js})
        }).catch(e => {
            console.log(e)
        })
    }
}