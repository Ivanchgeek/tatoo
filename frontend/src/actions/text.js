import {fetched} from "../utl/fetched"

export const GetDesktopText = () => {
    return dispatch => {
        fetched("/get/desktopText", {
            method: "get"
        }).then(resp => {
            if (!resp.ok) {
                throw resp.statusText
            }
            return resp.json()
        }).then(js => {
            dispatch({type: "SetDesktopText", ...js})
        }).catch(e => {
            console.log(e)
        })
    }
}

export const GetMobileText = () => {
    return dispatch => {
        fetched("/get/mobileText", {
            method: "get"
        }).then(resp => {
            if (!resp.ok) {
                throw resp.statusText
            }
            return resp.json()
        }).then(js => {
            dispatch({type: "SetMobileText", ...js})
        }).catch(e => {
            console.log(e)
        })
    }
}

export const GetLinks = () => {
    return dispatch => {
        fetched("/get/links", {
            method: "get"
        }).then(resp => {
            if (!resp.ok) {
                throw resp.statusText
            }
            return resp.json()
        }).then(js => {
            dispatch({type: "SetLinks", ...js})
        }).catch(e => {
            console.log(e)
        })
    }
}