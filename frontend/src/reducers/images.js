const initialState = {
    Images: []
}

export default function text(state = initialState, action) {
    switch (action.type) {
        case "SetImages":
            return (
                {
                    ...state,
                    Images: action.Images
                }
            )
        default:
            return state    
    }
}

