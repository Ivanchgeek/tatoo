const initialState = {
    HugeDesktopText: "Загрузка...",
    RegularDesktopText: "(br).(br)",
    HugeMobileText: "Загрузка...",
    RegularMobileText: " (br) ",
    Vk: "",
    Inst: "",
    EMail: "",
    PhoneFace: "Загрузка...",
    PhoneLink: "",
    AddressFace: "Загрузка...",
    AddressLink: ""
}

export default function text(state = initialState, action) {
    switch (action.type) {
        case "SetDesktopText":
            return (
                {
                    ...state,
                    HugeDesktopText: action.HugeDesktopText,
                    RegularDesktopText: action.RegularDesktopText
                }
            )
        case "SetMobileText":
            return (
                {
                    ...state,
                    HugeMobileText: action.HugeMobileText,
                    RegularMobileText: action.RegularMobileText
                }
            )
        case "SetLinks":
            return (
                {
                    ...state,
                    Vk: action.Vk,
                    Inst: action.Inst,
                    EMail: action.EMail,
                    PhoneFace: action.PhoneFace,
                    PhoneLink: action.PhoneLink,
                    AddressFace: action.AddressFace,
                    AddressLink: action.AddressLink
                }
            )
        default:
            return state    
    }
}

