import {combineReducers} from "redux"
import text from "./text"
import images from "./images"

export default combineReducers({
    text,
    images
})
