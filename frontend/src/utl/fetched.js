export const fetched = (url, params) => {
    if (process.env.NODE_ENV === "production") {
        url = window.location.origin + ":8080" + url
    }
    return fetch(url, params)
}

export const getApi = () => {
    if (process.env.NODE_ENV === "production") {
        return window.location.origin + ":8080"
    }
    return "http://" + window.location.hostname + ":8080"
}