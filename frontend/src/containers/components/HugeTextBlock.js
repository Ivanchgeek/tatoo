import React from "react"
import {connect} from "react-redux"
import {GetDesktopText, GetMobileText} from "../../actions/text"

class HugeTextBlock extends React.Component {

    componentDidMount = () => {
        if (this.props.mobile) {
            this.props.dispatch(GetMobileText())
        } else {
            this.props.dispatch(GetDesktopText())
        }
    }

    regularText = () => {
        var text = this.props.mobile ? this.props.RegularMobileText.split("(br)") : this.props.RegularDesktopText.split("(br)")
        var list = text.map((line, i) => {
            return (
                <p key={i}>{line}</p>
            )
        })
        return list
    }

    render() {
        return (
            <div className={`hugeTextBlock ${this.props.mobile ? "mobile" : ""}`} ref={this.props.refContainer}>
                <span>{this.props.mobile ? this.props.HugeMobileText : this.props.HugeDesktopText}</span><br/>
                <this.regularText></this.regularText>
            </div>
        )
    }

}

const mapStateToProps = (state) => ({
    HugeDesktopText: state.text.HugeDesktopText,
    RegularDesktopText: state.text.RegularDesktopText,
    HugeMobileText: state.text.HugeMobileText,
    RegularMobileText: state.text.RegularMobileText
});
export default connect(mapStateToProps)(HugeTextBlock)