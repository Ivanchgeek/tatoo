import React from "react"

class Progress extends React.Component {
    render() {
        return (
            <div className="progress">
                {/* {
                    this.props.photos.map((_, i) => {
                        return (
                            <div key={i} className={`point ${i === this.props.cursor ? "active" : ""}`}></div>
                        )
                    })
                } */}
                <div className="liquid" style={{width: `${this.props.cursor / this.props.photos.length * 100}%`}}></div>
            </div>
        )
    }
}

export default Progress