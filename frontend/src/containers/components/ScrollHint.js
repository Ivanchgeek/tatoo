import React from "react"

class ScrollHint extends React.Component {

    render() {
        return (
            <div className={`scrollHint ${this.props.mobile ? "mobile" : ""}`}>
                <h1>
                    Хочешь узнать больше?<br/>
                    Скроль вниз
                </h1>
                <div className="waterFall"></div>
            </div>
        )
    }

}

export default ScrollHint