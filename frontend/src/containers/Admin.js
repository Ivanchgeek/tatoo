import React from "react"
import {connect} from "react-redux"
import {GetDesktopText, GetMobileText, GetLinks} from "../actions/text"
import {fetched} from "../utl/fetched"
import {getApi} from "../utl/fetched";
import {LoadImagesList} from "../actions/images";

class Admin extends React.Component {

    constructor(props) {
        super(props)
        this.key = ""
        this.HUGE_DESKTOP_TEXT = React.createRef()
        this.REGULAT_DESKTOP_TEXT = React.createRef()
        this.HUGE_MOBILE_TEXT = React.createRef()
        this.REGULAT_MOBILE_TEXT = React.createRef()
        this.VK = React.createRef()
        this.INST = React.createRef()
        this.EMAIL = React.createRef()
        this.PHONE_FACE = React.createRef()
        this.PHONE_LINK = React.createRef()
        this.ADDRESS_FACE = React.createRef()
        this.ADDRESS_LINK = React.createRef()
        this.FILES = React.createRef()
        this.state = {
            preload: []
        }
    }

    componentDidMount = () => {
        this.key = prompt("enter access key")
        this.props.dispatch(GetMobileText())
        this.props.dispatch(GetDesktopText())
        this.props.dispatch(GetLinks())
        this.props.dispatch(LoadImagesList())
        document.title = "IvanTattoo (admin)"
        setTimeout(() => {
            this.HUGE_DESKTOP_TEXT.current.value = this.props.HugeDesktopText
            this.REGULAT_DESKTOP_TEXT.current.value = this.props.RegularDesktopText.replace(/\(br\)/gi, String.fromCharCode(13, 10))
            this.HUGE_MOBILE_TEXT.current.value = this.props.HugeMobileText
            this.REGULAT_MOBILE_TEXT.current.value = this.props.RegularMobileText.replace(/\(br\)/gi, String.fromCharCode(13, 10))
            this.VK.current.value = this.props.Vk
            this.INST.current.value = this.props.Inst
            this.EMAIL.current.value = this.props.EMail
            this.PHONE_FACE.current.value = this.props.PhoneFace
            this.PHONE_LINK.current.value = this.props.PhoneLink
            this.ADDRESS_FACE.current.value = this.props.AddressFace
            this.ADDRESS_LINK.current.value = this.props.AddressLink
            alert("ready")
        }, 500)
    }

    SetDesktopText = () => {
        fetched("/set/desktopText", {
            method: "post",
            body: JSON.stringify({
                AccessKey: this.key,
                HugeDesktopText: this.HUGE_DESKTOP_TEXT.current.value,
                RegularDesktopText: this.REGULAT_DESKTOP_TEXT.current.value.replace(/\n/gi, "(br)")
            })
        }).then(resp => {
            if (!resp.ok) {
                if (resp.status === 405) {
                    alert("wrong key")
                    throw new Error("access denied")
                } else {
                    alert("some erros occured")
                    throw resp.statusText
                }
            } else {
                alert("desktop text saved")
            }
        }).catch(e => {
            console.log(e)
        })
    }

    SetMobileText = () => {
        fetched("/set/mobileText", {
            method: "post",
            body: JSON.stringify({
                AccessKey: this.key,
                HugeMobileText: this.HUGE_MOBILE_TEXT.current.value,
                RegularMobileText: this.REGULAT_MOBILE_TEXT.current.value.replace(/\n/gi, "(br)")
            })
        }).then(resp => {
            if (!resp.ok) {
                if (resp.status === 405) {
                    alert("wrong key")
                    throw new Error("access denied")
                } else {
                    alert("some erros occured")
                    throw resp.statusText
                }
            } else {
                alert("mobile text saved")
            }
        }).catch(e => {
            console.log(e)
        })
    }

    SetLinks = () => {
        fetched("/set/links", {
            method: "post",
            body: JSON.stringify({
                AccessKey: this.key,
                Vk: this.VK.current.value,
                Inst: this.INST.current.value,
                EMail: this.EMAIL.current.value,
                PhoneFace: this.PHONE_FACE.current.value,
                PhoneLink: this.PHONE_LINK.current.value,
                AddressFace: this.ADDRESS_FACE.current.value,
                AddressLink: this.ADDRESS_LINK.current.value
            })
        }).then(resp => {
            if (!resp.ok) {
                if (resp.status === 405) {
                    alert("wrong key")
                    throw new Error("access denied")
                } else {
                    alert("some erros occured")
                    throw resp.statusText
                }
            } else {
                alert("links saved")
            }
        }).catch(e => {
            console.log(e)
        })
    }

    ShowFiles = () => {
        var preload = []
        for (var i = 0; i < this.FILES.current.files.length; i++) {
            preload.push(<p key={i}>{this.FILES.current.files[i].name}</p>)
        }
        this.setState({preload: preload})
    }

    LoadFiles = () => {
        var data = new FormData()
        var files = Array.from(this.FILES.current.files)
        data.append("length", files.length.toString())
        data.append("AccessKey", this.key)
        var size = 0
        for (var i = 0; i < files.length; i++) {
            data.append(i.toString(), files[i])
            size += files[i].size
        }
        if (size / 10**6 > 30) {
            alert(`Files size eroor\nmax: 30mb\ncurrent: ${size / 10**6}mb`)
            return
        }
        fetched("/loadImages", {
            method: "post",
            body: data
        }).then(resp => {
            if (!resp.ok) {
                if (resp.status === 405) {
                    alert("wrong key")
                    throw new Error("access denied")
                } else {
                    alert("some erros occured")
                    throw resp.statusText
                }
            } else {
                alert("images loaded")
                this.setState({preload: []})
                this.props.dispatch(LoadImagesList())
            }
        }).catch(e => {
            console.log(e)
        })
    }

    RemoveImage = event => {
        if (!window.confirm(`remove image ${event.target.dataset.image}?`)) {
            return
        }
        fetched("/remove/image", {
            method: "post",
            body: JSON.stringify({
                AccessKey: this.key,
                Image: event.target.dataset.image
            })
        }).then(resp => {
            if (!resp.ok) {
                if (resp.status === 405) {
                    alert("wrong key")
                    throw new Error("access denied")
                } else {
                    alert("some erros occured")
                    throw resp.statusText
                }
            }
            this.props.dispatch(LoadImagesList())
        }).catch(e => {
            console.log(e)
        })
    }

    MoveImage = event => {
        fetched("/move/image", {
            method: "post",
            body: JSON.stringify({
                AccessKey: this.key,
                Image: event.target.dataset.image,
                Direction: event.target.dataset.direction === "up"
            })
        }).then(resp => {
            if (!resp.ok) {
                switch (resp.status) {
                    case 405:
                        alert("wrong key")
                        throw new Error("access denied")
                    case 400:
                        console.error("no space")
                        break
                    default:
                        alert("some erros occured")
                        throw resp.statusText
                }
            }
            this.props.dispatch(LoadImagesList())
        }).catch(e => {
            console.log(e)
        })
    }

    render() {
        document.body.style.background = "white"
        return (
            <div className="admin">
                <div className="text">
                    <h3>Text settings</h3>
                    <p>HUGE_DESKTOP_TEXT</p>
                    <input type="text" ref={this.HUGE_DESKTOP_TEXT} onBlur={this.SetDesktopText}></input>
                    <p>REGULAT_DESKTOP_TEXT</p>
                    <textarea ref={this.REGULAT_DESKTOP_TEXT} onBlur={this.SetDesktopText}></textarea>
                    <p>HUGE_MOBILE_TEXT</p>
                    <input type="text" ref={this.HUGE_MOBILE_TEXT} onBlur={this.SetMobileText}></input>
                    <p>REGULAT_MOBILE_TEXT</p>
                    <textarea ref={this.REGULAT_MOBILE_TEXT} onBlur={this.SetMobileText}></textarea>

                    <h3>Links settings</h3>
                    <p>VK</p>
                    <input type="text" ref={this.VK} onBlur={this.SetLinks}></input>
                    <p>INST</p>
                    <input type="text" ref={this.INST} onBlur={this.SetLinks}></input>
                    <p>E-MAIL</p>
                    <input type="text" ref={this.EMAIL} onBlur={this.SetLinks}></input>
                    <p>PHONE_FACE</p>
                    <input type="text" ref={this.PHONE_FACE} onBlur={this.SetLinks}></input>
                    <p>PHONE_LINK</p>
                    <input type="text" ref={this.PHONE_LINK} onBlur={this.SetLinks}></input>
                    <p>ADDRESS_FACE</p>
                    <input type="text" ref={this.ADDRESS_FACE} onBlur={this.SetLinks}></input>
                    <p>ADDRESS_LINK</p>
                    <input type="text" ref={this.ADDRESS_LINK} onBlur={this.SetLinks}></input>

                    <h3>Galary settings</h3>
                    <input type="file" multiple ref={this.FILES} onClick={this.ClearFiles} onChange={this.ShowFiles}></input>
                    {this.state.preload}
                    <p></p>
                    <button className="last" onClick={this.LoadFiles}>SEND</button>
                </div>
                <div className="images">
                    {this.props.Images.map((image, i) => {
                        return (
                            <div className={`img-wrap ${this.props.Images.length - 1 === i ? "last" : ""}`} key={i}>
                                <img src={getApi() + "/image/" + image} alt={image}></img>
                                <div className="toolbar">
                                    <span data-image={image} onClick={this.RemoveImage}>🗙</span>
                                    <span data-image={image} data-direction="up" onClick={this.MoveImage}>⇧</span>
                                    <span data-image={image} data-direction="down" onClick={this.MoveImage}>⇩</span>
                                </div>
                            </div>
                        )
                    })}
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    HugeDesktopText: state.text.HugeDesktopText,
    RegularDesktopText: state.text.RegularDesktopText,
    HugeMobileText: state.text.HugeMobileText,
    RegularMobileText: state.text.RegularMobileText,
    Vk: state.text.Vk,
    Inst: state.text.Inst,
    EMail: state.text.EMail,
    PhoneFace: state.text.PhoneFace,
    PhoneLink: state.text.PhoneLink,
    AddressFace: state.text.AddressFace,
    AddressLink: state.text.AddressLink,
    Images: state.images.Images
});
export default connect(mapStateToProps)(Admin)