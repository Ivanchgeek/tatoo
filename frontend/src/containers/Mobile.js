import React from "react"
import MainPageMobile from "./views/MainPageMobile"
import GalatyMobile from "./views/GalaryMobile"
import Form from "./views/Form"
import Footer from "./views/Footer"

class Mobile extends  React.Component {

    render() {
        return (
            <div className="mobile">
               <MainPageMobile></MainPageMobile>
               <GalatyMobile></GalatyMobile>
               <Form></Form> 
               <Footer></Footer>
            </div>
        )
    }

}

export default Mobile