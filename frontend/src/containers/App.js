import React from 'react';
import "../styles/App.sass"
import {BrowserView, MobileView} from "react-device-detect";
import Desktop from "./Desktop"
import Mobile from "./Mobile"
import Admin from "./Admin"
import { BrowserRouter, Route, Switch } from 'react-router-dom';

class App extends React.Component {

  componentDidMount = () => {
    require('viewport-units-buggyfill').init()
  }

  render() {
    return (
      <>
        <BrowserView>
          <BrowserRouter>
            <Switch>
              <Route path='/admin' component={Admin}/> 
              <Route path='/' component={Desktop} />   
            </Switch>
          </BrowserRouter>
        </BrowserView>

        <MobileView><Mobile></Mobile></MobileView>
      </>
    )
  }
}

export default App;
