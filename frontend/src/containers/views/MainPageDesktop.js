import React from "react" 
import {connect} from "react-redux"
import logo from "../../res/logo.svg"
import ScrollHint from "../components/ScrollHint"
import HugeTextBlock from "../components/HugeTextBlock"

class MainPageDesktop extends React.Component {
    
    constructor(props) {
        super(props)
        this.mainText = React.createRef()
        this.orderButton = React.createRef()
        this.state = {
            mainTextStyle: {
                display: "none",
                widthNum: 0,
                heightNum: 0
            }
        }
    }

    componentDidMount = () => {
        this.drawLine()
    }

    drawLine = () => {
        if (this.props.HugeDesktopText !== "Загрузка...") {
            let textRect = this.mainText.current.getBoundingClientRect()
            let orderRect = this.orderButton.current.getBoundingClientRect()
            this.setState({
                mainTextStyle: {
                    display: "block",
                    top: `${textRect.top + (window.pageYOffset || document.documentElement.scrollTop) + textRect.height / 4 * 3}px`,
                    left: `${textRect.left + textRect.width}px`,
                    height: `${orderRect.top + (window.pageYOffset || document.documentElement.scrollTop) + orderRect.height / 2 - (textRect.top + (window.pageYOffset || document.documentElement.scrollTop) + textRect.height / 4 * 3)}px`,
                    width: `${orderRect.left - (textRect.left + textRect.width)}px`,
                    heightNum: orderRect.top + (window.pageYOffset || document.documentElement.scrollTop) + orderRect.height / 2 - (textRect.top + (window.pageYOffset || document.documentElement.scrollTop) + textRect.height / 4 * 3),
                    widthNum: orderRect.left - (textRect.left + textRect.width),
                    clipPath: "polygon(0 0, 0 0, 0 100%, 0% 100%)"
                }
            })
            setTimeout(() => {
                this.setState({
                    mainTextStyle: {
                        ...this.state.mainTextStyle,
                        clipPath: "polygon(0 -150%, 100% -150%, 100% 250%, 0% 250%)"
                    }
                })
            }, 500)
        } else {
            setTimeout(this.drawLine, 100)
        }
    }

    render() {
        return (
            <div className="mainPage section">
                <img src={logo} className="logo" alt="logo"></img>
                <HugeTextBlock refContainer={this.mainText}></HugeTextBlock>
                <a className="order" ref={this.orderButton} href="#form">Жми сюда</a>
                <svg className="line" fill="none" xmlns="http://www.w3.org/2000/svg" style={this.state.mainTextStyle}>
                    <path 
                    d={`M10 0 C${this.state.mainTextStyle.widthNum / 3} ${-this.state.mainTextStyle.heightNum} ${this.state.mainTextStyle.widthNum / 3 * 2} ${this.state.mainTextStyle.heightNum * 2} ${this.state.mainTextStyle.widthNum - 10} ${this.state.mainTextStyle.heightNum}`} 
                        stroke="white" strokeWidth="8" strokeDasharray="5 5">
                            <animate attributeName="stroke-dasharray" dur=".5s" values="4 5 5; 5 5 5" fill="freeze" repeatCount="indefinite" begin=".9s"/>
                    </path>
                </svg>
                <ScrollHint></ScrollHint>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    HugeDesktopText: state.text.HugeDesktopText
});
export default connect(mapStateToProps)(MainPageDesktop)
