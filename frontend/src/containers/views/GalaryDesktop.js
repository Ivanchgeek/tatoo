import React from "react" 
import {connect} from "react-redux"
import Progress from "../components/Progress"
import {LoadImagesList} from "../../actions/images"
import load from "../../res/load.png"
import {getApi} from "../../utl/fetched"

class GalaryDesktop extends React.Component {

    constructor(props) {
        super(props)
        this.underleft = React.createRef()
        this.left = React.createRef()
        this.center = React.createRef()
        this.right = React.createRef()
        this.underright = React.createRef()
        this.closeupView = React.createRef()
        this.photos = [load, load, load, load, load]
        this.state = {
            currentPhoto: 0,
            sidework: false,
            safeFrames: false,
            closeup: false
        }
    }

    componentDidMount = () => {
        this.props.dispatch(LoadImagesList())
    }

    goleft = () => {
        if (!this.state.sidework) {
            this.right.current.classList.replace("right", "center")
            this.center.current.classList.replace("center", "left")
            setTimeout(() => {
                this.setState({
                    sidework: true,
                    safeFrames: true
                })
                this.left.current.childNodes[0].src = this.center.current.childNodes[0].src
                setTimeout(() => {
                    this.left.current.style.zIndex = "2"
                    setTimeout(() => {
                        this.center.current.childNodes[0].src = this.right.current.childNodes[0].src
                        setTimeout(() => {
                            this.center.current.classList.replace("left", "center")
                            setTimeout(() => {
                                this.center.current.style.zIndex = "2"
                                setTimeout(() => {
                                    this.right.current.childNodes[0].src = this.underright.current.childNodes[0].src
                                    setTimeout(() => {
                                        this.right.current.classList.replace("center", "right")
                                        setTimeout(() => {
                                            this.underleft.current.style.zIndex = "0"
                                            this.left.current.style.zIndex = "0"
                                            this.center.current.style.zIndex = "0"
                                            this.right.current.style.zIndex = "1"
                                            this.underright.current.style.zIndex = "0"
                                            setTimeout(() => {
                                                this.setState({
                                                    currentPhoto: this.state.currentPhoto === this.photos.length - 1 ? 0 : this.state.currentPhoto + 1
                                                })
                                                setTimeout(() => {
                                                    this.setState({
                                                        sidework: false, 
                                                        safeFrames:false
                                                    })
                                                }, 1)
                                            }, 1)
                                        }, 1)
                                    }, 1)
                                }, 1)
                            }, 1)
                        }, 1)
                    }, 1)
                }, 1)
            }, 1001)
        }
    }

    goright = () => {
        if (!this.state.sidework) {
            this.left.current.classList.replace("left", "center")
            this.center.current.style.zIndex = "3"
            this.center.current.classList.replace("center", "right")
            setTimeout(() => {
                this.setState({
                    sidework: true,
                    safeFrames: true
                })
                this.right.current.childNodes[0].src = this.center.current.childNodes[0].src
                setTimeout(() => {
                    this.right.current.style.zIndex = "2"
                    setTimeout(() => {
                        this.center.current.childNodes[0].src = this.left.current.childNodes[0].src
                        setTimeout(() => {
                            this.center.current.classList.replace("right", "center")
                            setTimeout(() => {
                                this.center.current.style.zIndex = "2"
                                setTimeout(() => {
                                    this.left.current.childNodes[0].src = this.underleft.current.childNodes[0].src
                                    setTimeout(() => {
                                        this.left.current.classList.replace("center", "left")
                                        setTimeout(() => {
                                            this.underleft.current.style.zIndex = "0"
                                            this.left.current.style.zIndex = "0"
                                            this.center.current.style.zIndex = "0"
                                            this.right.current.style.zIndex = "1"
                                            this.underright.current.style.zIndex = "0"
                                            setTimeout(() => {
                                                this.setState({
                                                    currentPhoto: this.state.currentPhoto === 0 ? this.photos.length - 1 : this.state.currentPhoto - 1
                                                })
                                                setTimeout(() => {
                                                    this.setState({
                                                        sidework: false, 
                                                        safeFrames:false
                                                    })
                                                }, 1)
                                            }, 1)
                                        }, 1)
                                    }, 1)
                                }, 1)
                            }, 1)
                        }, 1)
                    }, 1)
                }, 1)
            }, 1001)
        }
    }

    toggleCloseup = () => {
        if (!this.state.closeup) {
            this.closeupView.current.src = this.center.current.childNodes[0].src
            document.body.style.overflowY = "hidden"
        } else {
            document.body.style.overflowY = "scroll"
        }
        setTimeout(() => {
            this.setState({
                closeup: !this.state.closeup
            })
        }, 1)
    }

    render() {
        const {currentPhoto, sidework, safeFrames, closeup} = this.state
        var currentFrames = []
        this.photos = []
        for (var i = 0; i < this.props.Images.length; i++) {
            this.photos.push(getApi() + "/image/" + this.props.Images[i])
        }
        if (safeFrames) {
            currentFrames.push(this.underleft.current.childNodes[0].src)
            currentFrames.push(this.left.current.childNodes[0].src)
            currentFrames.push(this.center.current.childNodes[0].src)
            currentFrames.push(this.right.current.childNodes[0].src)
            currentFrames.push(this.underright.current.childNodes[0].src)
            console.log(currentFrames)
        }
        return (
            <div className="galary section">

                <div className={`curtain section ${closeup ? "active" : ""}`}>
                    <img ref={this.closeupView} alt="closeup" onClick={this.toggleCloseup}
                        style={{
                            display: closeup ? "block" : "none"
                        }}></img>
                </div>

                <h1 className="heading">Вот некоторые мои работы</h1>

                <div className={`view ${sidework ? "sidework" : "animate"}`}>
                    <div className="photo left" ref={this.underleft}>
                        <img className="vertical" 
                        src={safeFrames ? currentFrames[0] : (currentPhoto - 2 >= 0 ? this.photos[currentPhoto - 2] : this.photos[this.photos.length + (currentPhoto - 2)])} alt="0"></img>
                    </div>
                    <div className="photo left" ref={this.left} onClick={this.goright}>
                        <img className="vertical" 
                        src={safeFrames ? currentFrames[1] : (currentPhoto - 1 >= 0 ? this.photos[currentPhoto - 1] : this.photos[this.photos.length + (currentPhoto - 1)])} alt="1"></img>
                    </div>
                    <div className="photo center" ref={this.center} onClick={this.toggleCloseup}>
                        <img className="vertical" src={safeFrames ? currentFrames[2] : (this.photos[currentPhoto])} alt="2"></img>
                    </div>
                    <div className="photo right" ref={this.right} style={{zIndex: "1"}} onClick={this.goleft}>
                        <img className="vertical" 
                        src={safeFrames ? currentFrames[3] : (currentPhoto + 1 < this.photos.length ? this.photos[currentPhoto + 1] : this.photos[currentPhoto + 1 - this.photos.length])} alt="3"></img>
                    </div>
                    <div className="photo right" ref={this.underright}>
                        <img className="vertical" 
                        src={safeFrames ? currentFrames[4] : (currentPhoto + 2 < this.photos.length ? this.photos[currentPhoto + 2] : this.photos[currentPhoto + 2 - this.photos.length])} alt="4"></img>
                    </div>
                </div>

                <Progress cursor={currentPhoto} photos={this.photos}></Progress>

            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    Images: state.images.Images
});
export default connect(mapStateToProps)(GalaryDesktop)