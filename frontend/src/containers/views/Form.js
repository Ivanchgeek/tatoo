import React from "react"
import MaskInput from 'mask-input';
import success from "../../res/success.png"
import {fetched} from "../../utl/fetched"

class Form extends React.Component {

    constructor(props) {
        super(props)
        this.phone = React.createRef()
        this.name = React.createRef()
        this.state = {
            formSended: false,
            nameIncorrect: false,
            phoneIncorrent: false
        }
    }

    componentDidMount = () => {
        new MaskInput(this.phone.current, {
            mask: '+7 (000) 000-00-00',
            alwaysShowMask: true,
            maskChar: '•',
        });
    }

    phoneMatcher = () => {
        this.setState({
            phoneIncorrent: false
        })
        var match = this.phone.current.value.match(/([0-9]){1}/g)
        var position = match.length <= 1 ? 4 : match.length - 1
        if (match.length <= 1) {
            position = 4
        } else {
            switch (true) {
                case (match.length <= 4):
                    position = match.length + 3
                    break;
                case (match.length <= 7):
                    position = match.length + 5
                    break;
                case (match.length <= 9):
                    position = match.length + 6
                    break;
                case (match.length <= 11):
                    position = match.length + 7
                    break;
                default:
                    position = 4
            }
        }
        if (this.phone.current.setSelectionRange) {
            setTimeout(() => {
                this.phone.current.setSelectionRange(position, position);
            }, 100)
        } else if (this.phone.current.createTextRange) {
            setTimeout(() => {
                var range = this.phone.current.createTextRange();
                range.collapse(true);
                range.moveEnd('character', position);
                range.moveStart('character', position);
                range.select();
            }, 100)
        }
    }

    nameMatcher = () => {
        this.setState({
            nameIncorrect: false
        })
    }

    send = () => {
        let phoneIncorrent = !/^((\+7)+ \(([0-9]){3}\)+ ([0-9]){3}-+([0-9]){2}-+([0-9]){2})$/.test(this.phone.current.value)
        let nameIncorrect = this.name.current.value.length === 0
        if (!(phoneIncorrent || nameIncorrect)) {
            fetched("/sendOrder", {
                method: "post",
                body: JSON.stringify({
                    Name: this.name.current.value,
                    Phone: this.phone.current.value
                })
            }).then(resp => {
                if (resp.ok) {
                    this.setState({
                        phoneIncorrent: phoneIncorrent,
                        nameIncorrect: nameIncorrect,
                        formSended: true
                    })
                } else {
                    alert("Ошибка отправки, попробуйте позже.")
                    window.location.reload()
                }
            })
        } else {
            this.setState({
                phoneIncorrent: phoneIncorrent,
                nameIncorrect: nameIncorrect,
                formSended: false
            })
        }
    }

    vk = () => {
        window.open("https://vk.com/write2007440", '_blank').focus()
    }

    render() {
        return (
            <div className="form section">
                <a className="anchor" name="form" href=".">.</a>
                <div className={`formWrap ${this.state.formSended ? "hidden" : ""}`}>
                    <div className="textInput">
                        <div className="label">Имя</div>
                        <input autoComplete="on" type="text" ref={this.name} className={`textField ${this.state.nameIncorrect ? "incorrect" : ""}`} placeholder="Вячеслав" onFocus={this.nameMatcher} id="name"></input>
                    </div>
                    <div className="textInput">
                        <div className="label">Телефон</div>
                        <input autoComplete="on" type="text" className={`textField ${this.state.phoneIncorrent ? "incorrect" : ""}`} ref={this.phone} onFocus={this.phoneMatcher} id="phone"></input>
                    </div>
                    <button className="send" onClick={this.send}>Отправить</button>
                    <button className="vk" onClick={this.vk}>Записаться черерез ВК</button>
                </div>
                <img alt="success" className={`success ${this.state.formSended ? "visible" : ""}`} src={success}></img>
            </div>
        )
    }

}

export default Form