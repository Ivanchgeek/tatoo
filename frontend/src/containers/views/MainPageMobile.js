import React from "react"
import gun from "../../res/gun.jpg"
import ScrollHint from "../components/ScrollHint"
import HugeTextBlock from "../components/HugeTextBlock"

class MainPageMobile extends  React.Component {

    render() {
        return (
            <div className="mainPage section">
                <div className="image">
                    <img alt="tatto bg" src={gun}></img>
                </div>
                <HugeTextBlock mobile></HugeTextBlock>
                <a className="order" href="#form">Запись</a>
                <ScrollHint mobile></ScrollHint>
            </div> 
        )
    }

}

export default MainPageMobile