import React from "react"
import {connect} from "react-redux"
import {GetLinks} from "../../actions/text"
import vk from "../../res/vk.svg"
import email from "../../res/email.svg"
import instagram from "../../res/instagram.svg"

class Footer extends React.Component {

    componentDidMount = () => {
        this.props.dispatch(GetLinks())
    }

    render() {
        return(
            <>
                <div className="footer">
                    <a href={this.props.Vk} target="_blank" rel="noopener noreferrer">
                        <img src={vk} alt="VK"></img>
                    </a>
                    <a href={this.props.Inst} target="_blank" rel="noopener noreferrer">
                        <img src={instagram} alt="instagram"></img>
                    </a>
                    <a href={this.props.EMail} target="_blank" rel="noopener noreferrer">
                        <img src={email} alt="email"></img>
                    </a>
                </div>
                <div className="footer">
                    <a href={this.props.PhoneLink} target="_blank" rel="noopener noreferrer" className="text">
                        {this.props.PhoneFace}
                    </a>
                    <a href={this.props.AddressLink} target="_blank" rel="noopener noreferrer" className="text">
                        {this.props.AddressFace}
                    </a>
                </div>
            </>
        )
    }

}

const mapStateToProps = (state) => ({
    Vk: state.text.Vk,
    Inst: state.text.Inst,
    EMail: state.text.EMail,
    PhoneFace: state.text.PhoneFace,
    PhoneLink: state.text.PhoneLink,
    AddressFace: state.text.AddressFace,
    AddressLink: state.text.AddressLink
});
export default connect(mapStateToProps)(Footer)