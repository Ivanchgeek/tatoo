import React from "react"
import {connect} from "react-redux"
import Progress from "../components/Progress"
import {LoadImagesList} from "../../actions/images"
import load from "../../res/load.png"
import {getApi} from "../../utl/fetched"

class GalaryMobile extends React.Component {

    constructor(props) {
        super(props)
        this.faceFrame = React.createRef()
        this.fakeFrame = React.createRef()
        this.photos = [load, load, load, load, load]
        this.state = {
            currentPhoto: 0,
            swipePositive: false,
            safeFrames: false
        }
    }

    componentDidMount = () => {
        this.props.dispatch(LoadImagesList())
    }

    swipeManager = event => {
        switch (event.type) {
            case "touchstart":
                this.start = event.touches[0].clientX
                break;
            case "touchmove":
                this.setState({swipePositive: event.touches[0].clientX - this.start < 0})
                this.faceFrame.current.style.marginLeft = `${event.touches[0].clientX - this.start}px`
                break;
            case "touchend":
                if (Math.abs(parseInt(this.faceFrame.current.style.marginLeft.match(/\d+/g)[0])) > document.documentElement.clientWidth / 4) {
                    this.faceFrame.current.style.transition = ".1s"
                    this.setState({safeFrames: true})
                    this.faceFrame.current.src = this.fakeFrame.current.src
                    if (this.state.swipePositive) {
                        this.faceFrame.current.style.marginLeft = "-100%"
                        setTimeout(() => {
                            this.setState({currentPhoto: this.state.currentPhoto + 1 === this.photos.length ? 0 : this.state.currentPhoto + 1})
                            setTimeout(() => {
                                this.faceFrame.current.style.marginLeft = "0px"
                                this.setState({safeFrames: false})
                            }, 1)
                        }, 101)
                    } else {
                        this.faceFrame.current.style.marginLeft = "100%"
                        setTimeout(() => {
                            this.setState({currentPhoto: this.state.currentPhoto - 1 < 0 ? this.photos.length - 1 : this.state.currentPhoto - 1})
                            setTimeout(() => {
                                this.faceFrame.current.style.marginLeft = "0px"
                                this.setState({safeFrames: false})
                            }, 1)
                        }, 101)
                    }
                    setTimeout(() => {
                        this.faceFrame.current.style.transition = ".0s"
                    }, 100)
                } else {
                    this.faceFrame.current.style.transition = ".1s"
                    this.faceFrame.current.style.marginLeft = "0px"
                    setTimeout(() => {
                        this.faceFrame.current.style.transition = ".0s"
                    }, 100)
                }
                break;
            default:
                console.error("bad event type")
        }
    }

    render() {
        const {currentPhoto, swipePositive, safeFrames} = this.state
        this.photos = []
        for (var i = 0; i < this.props.Images.length; i++) {
            this.photos.push(getApi() + "/image/" + this.props.Images[i])
        }
        var currentFrames = []
        if (safeFrames) {
            currentFrames.push(this.fakeFrame.current.src)
            currentFrames.push(this.faceFrame.current.src)
        }
        return(
            <div className="galary section">
                <h1 className="heading">Вот некоторые мои работы</h1>
                <div className="frame" onTouchStart={this.swipeManager} onTouchMove={this.swipeManager} onTouchEnd={this.swipeManager}>
                    <img alt="frame" 
                        src={safeFrames ? currentFrames[0] : (swipePositive ? (currentPhoto + 1 === this.photos.length ? this.photos[0] : this.photos[currentPhoto + 1]) : (currentPhoto - 1 === -1 ? this.photos[this.photos.length - 1] : this.photos[currentPhoto - 1]))} ref={this.fakeFrame}></img>
                    <img alt="fake frame" src={safeFrames ? currentFrames[1] : this.photos[currentPhoto]} ref={this.faceFrame}></img>
                </div>
                <Progress cursor={currentPhoto} photos={this.photos}></Progress>
            </div>
        )
    }

}

const mapStateToProps = (state) => ({
    Images: state.images.Images
});
export default connect(mapStateToProps)(GalaryMobile)