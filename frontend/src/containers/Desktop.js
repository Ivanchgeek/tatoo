import React from "react"
import MainPageDesktop from "./views/MainPageDesktop"
import GalaryDesktop from "./views/GalaryDesktop"
import Form from "./views/Form"
import Footer from "./views/Footer"

class Desktop extends React.Component {

    render() {
        return (
            <div className="desktop">
                <MainPageDesktop></MainPageDesktop>
                <GalaryDesktop></GalaryDesktop>
                <Form></Form>
                <Footer></Footer>
            </div>
        )
    }
}

export default Desktop